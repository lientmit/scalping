//+------------------------------------------------------------------+
//|                                                     Scalping.mq5 |
//|                                  Copyright 2022, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2022, MetaQuotes Ltd."
#property link "https://www.mql5.com"
#property version "1.01"

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
#include <FindLevels.mqh>

#include <Trade/Trade.mqh>

CTrade trade;
#include <Trade\PositionInfo.mqh>

CPositionInfo m_position;
#include <ChartObjects\ChartObjectsTxtControls.mqh>

CChartObjectButton b;

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
input int DoCaoCuaThanhNen = 60; // độ cao của thanh nến sẽ vào lệnh (Points)
input double StopLoss = 48;      // StopLoss in Points

input int ChieuSau = 12;   // khoảng cách giá đặt và giá hiện tại
input int MaxSpreads = 12; // spreads tối đa

// input double TrailingStop = 100; //TralStart in points
// input double TrailingStep = 50;//TralStep in points
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnInit()
  {
   Print("Do cao thanh nen: " + DoubleToString(DoCaoCuaThanhNen * Point(), Digits()));
   if(!b.Create(0, "__close__", 0, 10, 500, 150, 35) ||
      !b.Color(clrWhiteSmoke) ||
      !b.BackColor(clrGray) ||
      !b.FontSize(10) ||
      !b.Description("Clean"))
      return INIT_FAILED;

   Reset();
   if(init_Levels() == INIT_FAILED)
      return (INIT_FAILED);

   return (INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OnChartEvent(const int id,
                  const long &lparam,
                  const double &dparam,
                  const string &sparam)
  {
   if(id == CHARTEVENT_OBJECT_CLICK && sparam == b.Name())
     {
      int obj_total = ObjectsTotal(0);
      if(obj_total > 0)
        {
         for(int i = 0; i < obj_total; i++)
           {
            string name = ObjectName(0, i);
            int match = StringFind(name, "disabled", 0);
            if(match > -1)
              {
               ObjectDelete(0, name);
              }
           }

        }
      Reset();
      Print("Clean");
     }
  }

//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   unInit_Levels();
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
string name_KhangCu = "";
string name_HoTro = "";

double price_KhangCu = -1;
double price_HoTro = -1;

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
datetime time_Cham_KhangCu = TimeCurrent();
datetime time_Cham_HoTro = TimeCurrent();

bool waiting_SellStop = false;
bool waiting_BuyStop = false;


input int ChanVeLaiKhiCachXPoints = 20;//chặn vẽ lại khi giá cách line X points

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OnTick()
  {
   if(!TerminalInfoInteger(TERMINAL_TRADE_ALLOWED))
      return;

   string comment = "";
   if(oldHighPrice != -1)
      comment +=" Khang cu: "+ DoubleToString(oldHighPrice, Digits())+"\n";
   if(oldLowPrice != -1)
      comment +=" Ho tro: "+ DoubleToString(oldLowPrice, Digits())+"\n";
   Comment(comment);


   if(PositionSelect(_Symbol) == true)
     {
      ModifyPosition();
      Reset();
      return;
     }
   else
      if(OrdersTotal() > 0)
        {
         ModifyOrder();
         Reset();
         return;
        }

   if(oldHighPrice > -1)
     {
      if(getAsk() >= oldHighPrice && !waiting_SellStop)
        {
         // giá đã chạm vào kháng cự
         // set biến để chờ mở lệnh SellStop
         Print("Gia da cham Khang Cu, dang cho mo lenh SELLSTOP");

         time_Cham_KhangCu = TimeCurrent();
         //name_KhangCu = ChangeToActive(name_KhangCu);
         waiting_SellStop = true;
        }

      if(waiting_SellStop)  // chờ để mở lệnh SellStop
        {
         bool res = OpenStopOrder(ORDER_TYPE_SELL_STOP); // get kết quả
         if(res)
           {
            // đã mở lệnh sellStop rồi, reset biến để tránh mở thêm lệnh
            //DisableHorizontalLine(name_KhangCu);
            waiting_SellStop = false;
            oldHighPrice= -1;
            // price_KhangCu = -1;
            // name_KhangCu = "";
            Print("Da mo lenh SELLSTOP thanh cong");
           }
        }

      if(waiting_SellStop)
        {
         // kiểm tra lại, nếu đã qua cây nến thứ 2 rồi thì hủy bỏ, sẽ chờ tới Line khác
         int count_candles = iBarShift(NULL, PERIOD_CURRENT, time_Cham_KhangCu, true);
         Print("count_candles=" + IntegerToString(count_candles));
         if(count_candles >= 1)
           {
            //DisableHorizontalLine(name_KhangCu);
            waiting_SellStop = false;
            oldHighPrice= -1;
            //price_KhangCu = -1;
            //name_KhangCu = "";
            Print("Qua thoi gian cho de mo lenh SELLSTOP");
           }
        }
     }

   if(oldLowPrice > -1)
     {
      if(getBid() <= oldLowPrice && !waiting_BuyStop)
        {
         // giá đã chạm vào hỗ trợ
         // set biến để chờ mở lệnh BuyStop
         Print("Gia da cham Ho Tro, dang cho mo lenh BUYSTOP");
         time_Cham_HoTro = TimeCurrent();
         //name_HoTro = ChangeToActive(name_HoTro);
         waiting_BuyStop = true;
        }

      if(waiting_BuyStop)  // chờ để mở lệnh BuyStop
        {
         bool res = OpenStopOrder(ORDER_TYPE_BUY_STOP); // get kết quả
         if(res)
           {
            // đã mở lệnh BuyStop rồi, reset biến để tránh mở thêm lệnh
            // DisableHorizontalLine(name_HoTro);
            waiting_BuyStop = false;
            oldLowPrice = -1;
            // name_HoTro = "";
            Print("Da mo lenh BUYSTOP thanh cong");
           }
        }

      if(waiting_BuyStop)
        {
         // kiểm tra lại, nếu đã qua cây nến thứ 2 rồi thì hủy bỏ, sẽ chờ tới Line khác
         int count_candles = iBarShift(NULL, PERIOD_CURRENT, time_Cham_HoTro, true);
         Print("count_candles=" + IntegerToString(count_candles));
         if(count_candles >= 1)
           {
            //DisableHorizontalLine(name_HoTro);
            waiting_BuyStop = false;
            oldLowPrice = -1;
            //name_HoTro = "";
            Print("Qua thoi gian cho de mo lenh BUYSTOP");
           }
        }
     }

   if(oldHighPrice > -1)
      if(getAsk() <= oldHighPrice + ChanVeLaiKhiCachXPoints * Point()
         && getAsk() >= oldHighPrice - ChanVeLaiKhiCachXPoints * Point())
        {
         return;
        }

   if(oldLowPrice > -1)
      if(getBid() <= oldLowPrice + ChanVeLaiKhiCachXPoints * Point()
         && getBid() >= oldLowPrice - ChanVeLaiKhiCachXPoints * Point())
        {
         return;
        }

   if(waiting_BuyStop || waiting_SellStop)
      return;

   if(PositionSelect(_Symbol) == true || OrdersTotal() > 0)
      return;

   drawLevels();
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void ChangeToPending(string name)
  {
   ObjectSetInteger(0, name, OBJPROP_STYLE, STYLE_SOLID);
   ObjectSetInteger(0, name, OBJPROP_COLOR, clrLime);
   ObjectSetInteger(0, name, OBJPROP_SELECTABLE, true);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string ChangeToActive(string name)
  {
   ObjectSetInteger(0, name, OBJPROP_COLOR, clrYellow);
   ObjectSetInteger(0, name, OBJPROP_STYLE, STYLE_SOLID);
   ObjectSetInteger(0, name, OBJPROP_SELECTABLE, true);
   string oldName = name;
   StringReplace(name, "line", "active");
   ObjectSetString(0, oldName, OBJPROP_NAME, name);
   Alert("Active");
   return name;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void DisableHorizontalLine(string name)
  {
   ObjectSetInteger(0, name, OBJPROP_STYLE, STYLE_DASH);
   ObjectSetInteger(0, name, OBJPROP_COLOR, clrYellow);
   ObjectSetInteger(0, name, OBJPROP_SELECTABLE, true);

   string oldName = name;
   StringReplace(name, "active", "disabled");
   Print("Line old =" + oldName + ", new name =" + name);
   ObjectSetString(0, oldName, OBJPROP_NAME, name);
   Alert("Disabled");
// redraw levels
// Reset();
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void Reset()
  {
   oldHighPrice = -1;
   oldLowPrice = -1;
   waiting_BuyStop = false;
   waiting_SellStop = false;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void ModifyPosition()
  {
   for(int i = 0; i < PositionsTotal(); i++)
     {
      ulong ticket = PositionGetTicket(i);
      string symbol = PositionGetString(POSITION_SYMBOL);
      double priceOpen = PositionGetDouble(POSITION_PRICE_OPEN);
      double priceStopLoss = PositionGetDouble(POSITION_SL);
      long magic = PositionGetInteger(POSITION_MAGIC);
      long orderType = PositionGetInteger(POSITION_TYPE);

      if(symbol == _Symbol)
        {

         if(orderType == POSITION_TYPE_BUY)
           {
            double orderStopLoss = NormalizeDouble(priceStopLoss, Digits());
            double newPriceStoploss = NormalizeDouble(getBid() - ChieuSau * Point(), Digits());
            if(!(orderStopLoss == 0.0 || newPriceStoploss > orderStopLoss))
               break;
            if(!trade.PositionModify(ticket, newPriceStoploss, 0))
              {
               Print("BUY Modify Error Code: " + IntegerToString(GetLastError()) +
                     " OP: " + DoubleToString(priceOpen, Digits()) +
                     " SL: " + DoubleToString(newPriceStoploss, Digits()) +
                     " Bid: " + DoubleToString(getBid(), Digits()) +
                     " Ask: " + DoubleToString(getAsk(), Digits()) +
                     " Spread: " + DoubleToString(getSpreads()));
              }
            else
               Print("BUY Modify OP: " + DoubleToString(priceOpen, Digits()) +
                     " SL: " + DoubleToString(newPriceStoploss, Digits()) +
                     " Bid: " + DoubleToString(getBid(), Digits()) +
                     " Ask: " + DoubleToString(getAsk(), Digits()) +
                     " Spread: " + DoubleToString(getSpreads()));
            break;
           }
         else
            if(orderType == POSITION_TYPE_SELL)
              {
               double orderStopLoss = NormalizeDouble(priceStopLoss, Digits());
               double newPriceStoploss = NormalizeDouble(getAsk() + ChieuSau * Point(), Digits());

               if(!((orderStopLoss == 0.0 || newPriceStoploss < orderStopLoss)))
                  break;
               if(!trade.PositionModify(ticket, newPriceStoploss, 0))
                 {
                  Print("SELL Modify Error Code: " + IntegerToString(GetLastError()) +
                        " OP: " + DoubleToString(priceOpen, Digits()) +
                        " SL: " + DoubleToString(newPriceStoploss, Digits()) +
                        " Bid: " + DoubleToString(getBid(), Digits()) +
                        " Ask: " + DoubleToString(getAsk(), Digits()) +
                        " Spread: " + DoubleToString(getSpreads()));
                 }
               else
                  Print("SELL Modify OP: " + DoubleToString(priceOpen, Digits()) +
                        " SL: " + DoubleToString(newPriceStoploss, Digits()) +
                        " Bid: " + DoubleToString(getBid(), Digits()) +
                        " Ask: " + DoubleToString(getAsk(), Digits()) +
                        " Spread: " + DoubleToString(getSpreads()));
               break;
              }
        }
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void ModifyOrder()
  {
   for(int i = 0; i < OrdersTotal(); i++)
     {
      ulong ticket = OrderGetTicket(i);
      string symbol = OrderGetString(ORDER_SYMBOL);
      double priceOpen = OrderGetDouble(ORDER_PRICE_OPEN);
      double priceStopLoss = OrderGetDouble(ORDER_SL);
      long magic = OrderGetInteger(ORDER_MAGIC);
      long orderType = OrderGetInteger(ORDER_TYPE);

      if(symbol == _Symbol)
        {
         if(orderType == ORDER_TYPE_BUY_STOP)
           {
            double orderOpenPrice = NormalizeDouble(priceOpen, Digits());
            double newOpenPrice = NormalizeDouble(getAsk() + ChieuSau * Point(), Digits());
            if(!((newOpenPrice < orderOpenPrice)))
               break;
            double newStoplossPrice = NormalizeDouble(newOpenPrice - (StopLoss + getSpreads()) * Point(), Digits());

            if(!trade.OrderModify(ticket, newOpenPrice, newStoplossPrice, 0, ORDER_TIME_DAY, NULL))
              {
               Print("BUYSTOP Modify Error Code: " + IntegerToString(GetLastError()) +
                     " OP: " + DoubleToString(newOpenPrice, Digits()) +
                     " SL: " + DoubleToString(newStoplossPrice, Digits()) +
                     " Bid: " + DoubleToString(getBid(), Digits()) +
                     " Ask: " + DoubleToString(getAsk(), Digits()));
              }

            break;
           }
         else
            if(orderType == ORDER_TYPE_SELL_STOP)
              {
               double orderOpenPrice = NormalizeDouble(priceOpen, Digits());
               double newOpenPrice = NormalizeDouble(getBid() - ChieuSau * Point(), Digits());
               if(!((newOpenPrice > orderOpenPrice)))
                  break;
               double newStoplossPrice = NormalizeDouble(newOpenPrice + (StopLoss + getSpreads()) * Point(), Digits());
               if(!trade.OrderModify(ticket, newOpenPrice, newStoplossPrice, 0, ORDER_TIME_DAY, NULL))
                 {
                  Print("SELLSTOP Modify Error Code: " + IntegerToString(GetLastError()) +
                        " OP: " + DoubleToString(newOpenPrice, Digits()) +
                        " SL: " + DoubleToString(newStoplossPrice, Digits()) +
                        " Bid: " + DoubleToString(getBid(), Digits()) +
                        " Ask: " + DoubleToString(getAsk(), Digits()));
                 }
              }
        }
     }
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double calcLots()
  {
   return 0.01;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool OpenStopOrder(int mode)
  {
   if(PositionSelect(_Symbol) == true || OrdersTotal() > 0)
      return false;

   double high_low = iHigh(NULL, 0, 0) - iLow(NULL, 0, 0);
   if(high_low <= DoCaoCuaThanhNen * Point())
     {
      Print("High Low invalid " + DoubleToString(high_low, Digits()));
      return false;
     }

   double lots = calcLots();

   if(mode == ORDER_TYPE_BUY_STOP)
     {
      if(getSpreads() < MaxSpreads)
        {
         double tempOpenPrice = NormalizeDouble(getAsk() + ChieuSau * Point(), Digits());
         double stoplossPrice = NormalizeDouble(tempOpenPrice - (StopLoss + getSpreads()) * Point(), Digits());

         if(!trade.BuyStop(lots, tempOpenPrice, _Symbol, stoplossPrice, 0, ORDER_TIME_DAY, 0, ""))
           {
            Print("BUYSTOP Send Error Code: " + IntegerToString(GetLastError()) +
                  " LT: " + DoubleToString(lots) +
                  " OP: " + DoubleToString(tempOpenPrice, Digits()) +
                  " SL: " + DoubleToString(stoplossPrice, Digits()) +
                  " Bid: " + DoubleToString(getBid(), Digits()) +
                  " Ask: " + DoubleToString(getAsk(), Digits()) +
                  " Spread: " + DoubleToString(getSpreads()));
           }
         else
            return true;
        }
     }
   else
     {
      if(getSpreads() < MaxSpreads)
        {
         double tempOpenPrice = NormalizeDouble(getBid() - ChieuSau * Point(), Digits());
         double stoplossPrice = NormalizeDouble(tempOpenPrice + (StopLoss + getSpreads()) * Point(), Digits());
         if(!trade.SellStop(lots, tempOpenPrice, _Symbol, stoplossPrice, 0, ORDER_TIME_DAY, 0, ""))
           {
            Print("SELLSTOP Send Error Code: " + IntegerToString(GetLastError()) +
                  " LT: " + DoubleToString(lots) +
                  " OP: " + DoubleToString(tempOpenPrice, Digits()) +
                  " SL: " + DoubleToString(stoplossPrice, Digits()) +
                  " Bid: " + DoubleToString(getBid(), Digits()) +
                  " Ask: " + DoubleToString(getAsk(), Digits()) +
                  " Spread: " + DoubleToString(getSpreads()));
           }
         else
            return true;
        }
     }

   return false;
  }
//+------------------------------------------------------------------+
long getSpreads()
  {
   return SymbolInfoInteger(_Symbol, SYMBOL_SPREAD);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getAsk()
  {
   return SymbolInfoDouble(_Symbol, SYMBOL_ASK);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getBid()
  {
   return SymbolInfoDouble(_Symbol, SYMBOL_BID);
  }
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
