//+------------------------------------------------------------------+
//|                                                      ProjectName |
//|                                      Copyright 2020, CompanyName |
//|                                       http://www.companyname.net |
//+------------------------------------------------------------------+
input group    "==== Find Levels settings ===="
input ENUM_TIMEFRAMES InPeriod  = PERIOD_CURRENT; // Timeframe
input int  InpMaxBars  =500; // Number of history bars in use
input int  InpDepth    =5;  // ZigZag Depth
input int  InpDeviation=5;   // ZigZag Deviation
input int  InpBackstep =3;   // ZigZag Back Step
input int  InpBackBars =1;   // Number of history bars indicating low/high
input group    "==== Find Levels settings ===="

double sellBuffers[], buyBuffers[];
int indiHandle;
double currHighPrice = 0, currLowPrice = 0;
double oldHighPrice  = 0, oldLowPrice  = 0;
string highLineName = "resistent_line";
string lowLineName  = "support_line";

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int init_Levels()
  {
   indiHandle = iCustom(_Symbol, InPeriod, "Examples\\ZigZag", InpDepth, InpDeviation, InpBackstep);
   if(indiHandle == INVALID_HANDLE)
     {
      printf("Error loading ZigZag indicator");
      return (INIT_FAILED);
     }

   return (INIT_SUCCEEDED);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void unInit_Levels()
  {
   IndicatorRelease(indiHandle);
   ObjectDelete(0, highLineName);
   ObjectDelete(0, lowLineName);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void drawLevels()
  {
   ArraySetAsSeries(sellBuffers, true);
   ArraySetAsSeries(buyBuffers, true);

   if(CopyBuffer(indiHandle, 1, 0, InpMaxBars, sellBuffers) <= 0)
     {
      printf("Error copying ZZ buffers");
      return;
     };
   if(CopyBuffer(indiHandle, 2, 0, InpMaxBars, buyBuffers) <= 0)
     {
      printf("Error copying ZZ buffers");
      return;
     };

// find previous HH, LL
   getHHLL();


// create high horizontal line
   if(currHighPrice != -1 && currHighPrice != oldHighPrice)
     {
      // delete old line
      ObjectDelete(0, highLineName);
      // create new line
      ObjectCreate(0, highLineName, OBJ_HLINE, 0, 0, currHighPrice);
      oldHighPrice = currHighPrice;
      
      
     }

// create low horizontal line
   if(currLowPrice != -1 && currLowPrice != oldLowPrice)
     {
      // delete old line
      ObjectDelete(0, lowLineName);
      // create new line
      ObjectCreate(0, lowLineName, OBJ_HLINE, 0, 0, currLowPrice);
      oldLowPrice = currLowPrice;

     }

  
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getHHLL()
  {
   int i, j;

   currHighPrice = -1;
   currLowPrice = -1;
   for(i=0; i<InpMaxBars; i++)
     {
      if(buyBuffers[i] != 0)
         break;
     }
   for(j=0; j<InpMaxBars; j++)
     {
      if(sellBuffers[j] != 0)
         break;
     }

// latest low
   if(i < j)
     {
      currHighPrice = sellBuffers[j];
      for(int ii=i + 1; ii<InpMaxBars; ii++)
        {
         double prevLow = buyBuffers[ii];
         if(prevLow != 0 && prevLow < buyBuffers[i])
            return (currLowPrice = prevLow);
        }
     }
// latest high
   if(j < i)
     {
      currLowPrice = buyBuffers[i];
      for(int jj= j+1; jj<InpMaxBars; jj++)
        {
         double prevHigh = sellBuffers[jj];
         if(prevHigh != 0 && prevHigh > sellBuffers[j])
            return (currHighPrice = prevHigh);
        }
     }

   return -1;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getResistentLine()
  {
   return oldHighPrice;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double getSupportLine()
  {
   return oldLowPrice;
  }
//+------------------------------------------------------------------+
